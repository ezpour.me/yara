from django.contrib import admin

from Store.models import Store, Product, File

admin.site.register(Store)
admin.site.register(Product)
admin.site.register(File)
