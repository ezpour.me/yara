from django.db import models

from Account.models import User


class Store(models.Model):
    user = models.ForeignKey(to=User, verbose_name='owner store', on_delete=models.CASCADE, related_name='stores')
    name = models.CharField(verbose_name='store name', max_length=150)
    created = models.DateTimeField(verbose_name='created', auto_now_add=True)
    activity_code = models.CharField(verbose_name='activity code', max_length=20)
    address = models.CharField(verbose_name='address', max_length=250)
    description = models.TextField(verbose_name='description', null=True, blank=True)

    class Meta:
        ordering = ['-created']
        verbose_name = 'store'
        verbose_name_plural = "stores"

    def __str__(self):
        return self.name


class Product(models.Model):
    store = models.ForeignKey(to=Store, verbose_name='store', on_delete=models.CASCADE, related_name='products')
    name = models.CharField(verbose_name='name', max_length=50)
    price = models.PositiveSmallIntegerField(verbose_name='price')
    count = models.PositiveSmallIntegerField(verbose_name='count')
    created = models.DateField(verbose_name='date create', auto_now_add=True)
    send_method = models.ManyToManyField(verbose_name='send method', to='SendMethod', through='ProductSend')

    class Meta:
        ordering = ['-created']
        verbose_name = 'product'
        verbose_name_plural = "products"

    def __str__(self):
        return self.name


class File(models.Model):
    product = models.ForeignKey(to=Product, verbose_name='product', on_delete=models.CASCADE, related_name='files')
    name = models.CharField(verbose_name='name', max_length=120)
    file_address = models.FileField(verbose_name='file')

    def __str__(self):
        return self.name


class ProductSend(models.Model):
    send_method = models.ForeignKey(verbose_name='send method', to='SendMethod', on_delete=models.CASCADE)
    product = models.ForeignKey(verbose_name='product', to=Product, on_delete=models.CASCADE)


class SendMethod(models.Model):
    name = models.CharField(verbose_name='send method', max_length=100)

    class Meta:
        ordering = ['name']

