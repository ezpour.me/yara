import uuid

from django.db import models

from Account.models import User
from Store.models import Product, ProductSend


def generate_unique_code():
    return uuid.uuid4().hex[:6].upper()


class Purchase(models.Model):
    user = models.ForeignKey(to=User, verbose_name='buyer user', on_delete=models.CASCADE, related_name='purchases')
    basket = models.ManyToManyField(to=Product, through='Basket')
    total_price = models.PositiveSmallIntegerField(verbose_name='total price')
    factor_code = models.CharField(verbose_name='factor code', max_length=6, default=generate_unique_code, unique=True)
    tracking_code = models.CharField(verbose_name='tracking code', max_length=25, null=True, blank=True)
    created = models.DateTimeField(verbose_name='register order', null=True)
    status = models.BooleanField(verbose_name='status payment', default=False)

    class Meta:
        ordering = ['-created']
        verbose_name = 'purchase'
        verbose_name_plural = "purchases"

    def __str__(self):
        return self.tracking_code


class Basket(models.Model):
    purchase = models.ForeignKey(to=Purchase, verbose_name='purchase', on_delete=models.CASCADE, related_name='baskets')
    product = models.ForeignKey(to=Product, verbose_name='product', on_delete=models.CASCADE, related_name='baskets')
    count = models.PositiveSmallIntegerField(verbose_name='count of product')
    send_method = models.ForeignKey(verbose_name='send method', to=ProductSend)
    created = models.DateTimeField(verbose_name='register in basket', auto_now_add=True)

    class Meta:
        ordering = ['-created']
        verbose_name = 'basket'
        verbose_name_plural = "baskets"
