from django.contrib import admin

from Factor.models import Purchase, Basket

admin.site.register(Purchase)
admin.site.register(Basket)